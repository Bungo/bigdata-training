# BigData-Training
All tasks were completed within the module "Big Data NoSQL" in the bachelor program. The language of course and thus also the language of documentation is German.<br>
All three tasks examined log files for properties of an RBDMS. The goal of the tasks was to learn how to handle Big Data with different Big Data / NoSQL technologies. Thereby properties like the throughput of the client or the latency of the requests were examined. Unfortunately, the examined data was only made available for a short time by the school and is therefore no longer available.
## Task 1 - Map Reduce
The first task examined the data with a MapReduce program on Hadoop (and YARN).
## Task 2 - Hive
For the second task the data had to be loaded with Hive and then examined.
## Task 3 - Cassandra
For the third task the data had to be loaded with Cassandra and then examined.