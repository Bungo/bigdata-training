from mrjob.job import MRJob
from mrjob.step import MRStep
import math

class MapReduce2(MRJob): 

	def steps(self):
		return [
		    MRStep(mapper=self.mapper,
		           reducer=self.reducerOne),
		    MRStep(combiner=self.combiner,
		           reducer=self.reducerTwo)
		]

	def mapper(self, _, line):
		list = line.split(",")
		if list[1] != 'client_id'and list[1] != "-1" and list[1] != "0":
                    yield str(list[1]) + ":" + str(list[2]), int(list[6])

	def reducerOne(self, key, values):
		values = list(values)
		yield math.ceil(min(values) / 60000.0), max(values) - min(values)
	
	def combiner(self, key, values):
		values = list(values)
		yield key, [sum(values), len(values)]

	def reducerTwo(self, key, values):
		values = list(zip(*list(values)))
		yield key, sum(values[0]) / sum(values[1])
		

if __name__ == '__main__':
	MapReduce2.run()
